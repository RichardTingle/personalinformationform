package com.company;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class PersonalInformationForm {

    private static void createAndShowGUI(FormLineItem... formLineItems) {
        JFrame frame = new JFrame("Personal Information Form");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        Container contentPane = frame.getContentPane();

        Supplier<Boolean> validateAndAttemptToCommit =
                addFormIntoContainer(contentPane, Arrays.asList(formLineItems));

        contentPane.add(Box.createVerticalStrut(20));

        JLabel successField = new JLabel(" ");
        successField.setForeground(new Color(0, 102, 0)); //dark green
        contentPane.add(successField);

        JButton submitButton = new JButton("Submit");

        submitButton.addActionListener(event -> {
            boolean valid = validateAndAttemptToCommit.get();
            if(valid) {
                successField.setText("Success! Data submitted");
            }

        });
        frame.getContentPane().add(submitButton);

        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Binds adds the form line items into the container, calling the Supplier will:
     * - Validate the form (showing validation errors on the form)
     * - If the form is valid push the data back to the bound object.
     * - Return if the form was valid.
     */
    static Supplier<Boolean> addFormIntoContainer(Container contentPaneToAddFormInto, Collection<FormLineItem> formLineItems){
        //Each of these when called updates the UI to show validation errors and returns if the field is valid
        Collection<Supplier<Boolean>> validation = new ArrayList<>();
        //these commit the data back to whatever its bound to
        Collection<Runnable> commitData = new ArrayList<>();

        for(FormLineItem lineItem : formLineItems){
            JLabel label = new JLabel(lineItem.getLabel());
            label.setAlignmentX(Component.LEFT_ALIGNMENT);
            contentPaneToAddFormInto.add(label);
            JTextField textField = new JTextField(50);
            textField.setAlignmentX(Component.LEFT_ALIGNMENT);
            contentPaneToAddFormInto.add(textField);
            JLabel validationErrorsLabel = new JLabel(" ");
            validationErrorsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
            validationErrorsLabel.setForeground(Color.RED);
            contentPaneToAddFormInto.add(validationErrorsLabel);

            Supplier<Boolean> validator = () -> {
                Collection<ValidationResult> validationErrors = lineItem.getValidators().stream()
                        .map(rule -> rule.validate(textField.getText()))
                        .filter(v -> !v.isValid())
                        .collect(Collectors.toList());
                validationErrorsLabel.setText(validationErrors.stream().map(ValidationResult::getMessage).collect(Collectors.joining(", ")));
                return validationErrors.isEmpty();
            };
            validation.add(validator);

            commitData.add(() -> lineItem.getDataBinding().accept(textField.getText()));
            contentPaneToAddFormInto.add(Box.createVerticalStrut(10)); //spacing between components
        }

        return () -> {
            boolean valid = true;
            //run the validation (can't use streams as we need the side effects)
            for(Supplier<Boolean> validator: validation){
                valid &= validator.get();
            }
            if(valid){
                commitData.forEach(Runnable::run);
            }
            return valid;
        };

    }

    public static void main(String[] args) {
        Consumer<String> justPrintBinding = System.out::println; //in a real application this would bind to something

        SwingUtilities.invokeLater(() -> createAndShowGUI(
                new FormLineItem("Given Name*", justPrintBinding, ValidationRule.REQUIRED),
                new FormLineItem("Surname*", justPrintBinding, ValidationRule.REQUIRED),
                new FormLineItem("Address line 1*", justPrintBinding, ValidationRule.REQUIRED),
                new FormLineItem("Address line", justPrintBinding),
                new FormLineItem("Town*", justPrintBinding, ValidationRule.REQUIRED),
                new FormLineItem("County", justPrintBinding, ValidationRule.REQUIRED),
                new FormLineItem("Country*", justPrintBinding, ValidationRule.REQUIRED),
                new FormLineItem("Postal Code*", justPrintBinding, ValidationRule.REQUIRED),
                new FormLineItem("Email Address*", justPrintBinding, ValidationRule.REQUIRED,ValidationRule.EMAIL ),
                new FormLineItem("Phone number", justPrintBinding, ValidationRule.PHONE_NUMBER )

        ));
    }



}
