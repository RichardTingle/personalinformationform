package com.company;

import java.util.regex.Pattern;

@FunctionalInterface
public interface ValidationRule {

    Pattern phoneNumber = Pattern.compile("\\+?[0-9]*");

    static ValidationRule REQUIRED = (String value) -> value.isEmpty()? ValidationResult.failure("Required") : ValidationResult.success();
    static ValidationRule PHONE_NUMBER = (String value) -> phoneNumber.matcher(value).matches() ? ValidationResult.success() : ValidationResult.failure("Phone number must contain only numbers (optionally prefixed by +)");
    static ValidationRule EMAIL = (String value) -> value.isEmpty() || value.contains("@")? ValidationResult.success(): ValidationResult.failure("Emails must have an @");

    ValidationResult validate(String value);
}
