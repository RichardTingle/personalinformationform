package com.company;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Consumer;

public class FormLineItem {

    private final String label;

    /**
     * Its assumed the form is bound to a java object, this binding sets the value
     */
    private final Consumer<String> dataBinding;

    private final Collection<ValidationRule> validators;

    public FormLineItem(String label, Consumer<String> dataBinding, ValidationRule... validators) {
        this.label = label;
        this.dataBinding = dataBinding;
        this.validators = Arrays.asList(validators);
    }

    public String getLabel() {
        return label;
    }

    public Consumer<String> getDataBinding() {
        return dataBinding;
    }

    public Collection<ValidationRule> getValidators() {
        return validators;
    }
}
