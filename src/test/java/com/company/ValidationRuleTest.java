package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ValidationRuleTest {

    @Test
    public void testEmailValidation(){
        assertTrue(ValidationRule.EMAIL.validate("test@email.com").isValid());
        assertFalse(ValidationRule.EMAIL.validate("testemailcom").isValid());
        assertTrue("Email validation doesn't imply required so empty string must pass validation", ValidationRule.EMAIL.validate("").isValid());
    }
    @Test
    public void testPhoneValidation(){
        assertTrue(ValidationRule.PHONE_NUMBER.validate("01234567890").isValid());
        assertTrue(ValidationRule.PHONE_NUMBER.validate("+441234567890").isValid());
        assertFalse(ValidationRule.PHONE_NUMBER.validate("notAPhoneNumber").isValid());
        assertTrue("Phone validation doesn't imply required so empty string must pass validation",ValidationRule.PHONE_NUMBER.validate("").isValid());
    }
    @Test
    public void testRequired(){
        assertTrue(ValidationRule.REQUIRED.validate("value").isValid());
        assertFalse(ValidationRule.REQUIRED.validate("").isValid());
    }
}