##Setup
This project is based on Gradle, open in any gradle aware IDE or run with Gradle directly.

##Run IDE
Run the main class PersonalInformationForm to start

##Run Gradle
- Within command line navigate to the root of the project
- Run the command `gradle run`

##Tests
Tests are based on jUnit

Tests can either be run within your IDE 

or 

The command `gradle test` can be run